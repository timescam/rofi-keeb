#!/bin/bash

bat=$(upower --dump | grep keyboard -A 7)
lay=$(setxkbmap -print -verbose 10)
battery=$(grep percentage <<< $bat)
life=$(grep "time to empty" <<< $bat)
layout=$(grep layout <<< $lay)
variant=$(grep variant <<< $lay)
caps=$(xset q | grep Caps)

title="⌨️"
widthpercent=28

typeset -A menu
menu=(
    [    $battery]=""
    [    $life]=""
    [    $layout]=""
    [    $variant]=""
    [$caps]=""
)

colors=(
    # uncomment to set custom colors
    # [-color-window]="#000000, #ffffff, #ffffff"
    # [-color-normal]="#ffffff, #000000, #eee8d5, #272727, #ffffff"
    # [-color-active]="#fdf6e3, #268bd2, #eee8d5, #268bd2, #fdf6e3"
    # [-color-urgent]="#fdf6e3, #dc322f, #eee8d5, #dc322f, #fdf6e3"
)