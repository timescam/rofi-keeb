# rofi-keeb

[rofi](https://github.com/davatorium/rofi) menu for keyboard status. I only make this extension for my Keychron K4,
but it should work with any keyboard in general as long as you can find it when running upower -d and using xset for keyboard layout.

## Scrennshots

with battery

`placeholder`

without battery

![example2](images/example2.png)

## Dependencies

[bash](https://www.gnu.org/software/bash/), 
[grep](http://man7.org/linux/man-pages/man1/grep.1.html),
[upower](https://manpage.me/?q=upower), 
[xset](https://www.x.org/archive/X11R7.5/doc/man/man1/xset.1.html),
these dependencies are pre-installed with most distros

[rofi](https://github.com/davatorium/rofi)

[rofigen](https://github.com/losoliveirasilva/rofigen), needed for menu genaration

## Getting Started

To run `rofi-keeb` you must pass the script through rofigen, like `./rofigen ~/keeb.sh`.

You can bindsym your [openbox](http://openbox.org/wiki/Main_Page)
```
<keybind key="C-W-l">
    <action name="Execute">
        <command>rofigen ~/keeb.sh</command>
    </action>
</keybind>
```

or w/ [i3wm](https://i3wm.org/),
```
bindsym $mod+Ctrl+l exec rofigen ~/keeb.sh
```


## Configuring

To edit the menu, youcan start from these:

- `title` title displayed
- `widthpercent` set width of menu, is specified in percentage (optional)
- `menu` menu items: [text]="command_to_execute"
- `colors` Rofi colors. You can use the [theme generator](https://davedavenport.github.io/rofi/p11-Generator.html) (optional)


## TODO

polybar module

~~stanalone script, not using rofigen~~ (now avalible on experimental branch)

## Credit

[Kurgol](https://github.com/Kurgol) for his [guide](https://github.com/Kurgol/keychron/blob/master/k2.md#battery)

[losoliveirasilva](https://github.com/losoliveirasilva) for [rofigen](https://github.com/losoliveirasilva/rofigen)

